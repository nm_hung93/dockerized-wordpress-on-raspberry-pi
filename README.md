# Dockerized Wordpress on Raspberry Pi

Use this repo to start your own Wordpress instance on your Raspberry Pi using Docker Containers. Every needed configuration file is already included in this project. A more throughout guide can be found here [https:/hungsblog.de/en](https://hungsblog.de/en/technology/1-installing-wordpress-on-a-raspberry-pi-with-docker-compose-in-under-10-minutes/).
### 1. Install Docker and Docker-Compose
>`$ curl -sSL https://get.docker.com | sh `

>`$ sudo apt-get install docker-compose  `
### 2. Clone this repo
>`$ git clone https://gitlab.com/nm_hung93/dockerized-wordpress-on-raspberry-pi.git`
### 3. Download Wordpress and configure the wp-config.php
>`$ chmod +x wordpress-install.sh `

>`$ ./wordpress-install.sh `

### 4. Run your docker-compose and access your installation
>`$ docker-compose up -d `

 Your credentials are as following:
Databasename: blog
Username: UserX
Password: 123456
Databasehost: mariadb 

# Next Steps
- play around with the configs of the data/wp/Caddyfile. 
- change initial Database created by the MariaDB image by changing the credentials in the file data/env_variables/mysql_credentials

# Docker Images

## Caddy v 1.0.3
Using the base alpine image, added Caddy and built it for ARM architecture

## PHP-FPM 8.0
Using the base PHP-FPM ARM image and added mysqli and some other extensions to work with Wordpress and MariaDB. Also changed the UID and GUID for both www-data to 33 to make it consistent with host system.

! Please take note, that the php image is now maintained in a separate [repository](https://gitlab.com/nm_hung93/php-wordpress-ready-armv7) !

## MariaDB 10.3.17
Used the image provided by  https://github.com/yobasystems/alpine-mariadb .



