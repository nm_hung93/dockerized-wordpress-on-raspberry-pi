<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'blog' );

/** MySQL database username */
define( 'DB_USER', 'UserX' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', 'mariadb' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'InKL2V)^a|=^J_F0#>`6 KF=yw?,RUxTL`q86fed)7L5~QCg[UAH)S,}`6s0cO=7' );
define( 'SECURE_AUTH_KEY',  't5)Z~VVN7.RtWj<?AzY1-+%w-b}#NONi?T/%gh%[S|T1Xs4:oe}thrgf|G};r;.p' );
define( 'LOGGED_IN_KEY',    'pxau.b25uH%R)yE0DCl@X_|kIZ1<>;CVW=si9_yzr?.UNgYp-PpSv9,5slz6UkyK' );
define( 'NONCE_KEY',        ',bh}J?/Qk45|=6QzJ^f4SkxjbX)5qQp@RjeY#w|cB[Z1$~n!PLc5Wo<,KX[?r`o,' );
define( 'AUTH_SALT',        'm*4X+Q%jD7cs[X{.}(PGlDW7C[b&9}]T`e2~x4v{h]b>[b(TbeNtp{mq G47-RW}' );
define( 'SECURE_AUTH_SALT', 'oe1xjJDf}2?-@bgu.26vy-}Clw[;2d:!IctA#z]`L` LW,ZLUK*}YGDK<i[bglW5' );
define( 'LOGGED_IN_SALT',   '})cMX_WL%Fa OQ#}9EB9_8:b@Q/%#w4YmvTv4_HbJVzH AE;;TNvXIb%T24MIYhs' );
define( 'NONCE_SALT',       'SP!:A| s[TLiHSmfv7Z0F1k^2p:f9}rI[UJ[agN (|_R|WMF.@Vy+?%:y{S&AU j' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
