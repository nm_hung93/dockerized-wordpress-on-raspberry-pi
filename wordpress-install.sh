#!/bin/bash

echo "##### Downloading latest Wordpress Version #####"
curl -sL http://wordpress.org/latest.tar.gz | tar --strip 1 -xz -C data/wp/www
echo "##### Download finished, copy preconfigured wp-config.php into data/wp/www #####"
cp wp-config.php data/wp/www/
